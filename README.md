# README #

This is the repository for the Hypnos web service, you do not need to run nothing to use the Hypnos Android Application (https://bitbucket.org/edoardoantonini722/hypnos-app), because the web service is already up remotely.

But, if you want, there's a runnable jar in the downloads section, it will work if you have the Hypnos database in localhost:5432.

For any problem you can mail at edoardo.antonini@studio.unibo.it