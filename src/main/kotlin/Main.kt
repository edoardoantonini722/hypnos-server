import utils.JdbiConfiguration

/**
 * The starting point of the application
 */

fun main(args: Array<String>) {
    RouteController.init()
    JdbiConfiguration.init()
}