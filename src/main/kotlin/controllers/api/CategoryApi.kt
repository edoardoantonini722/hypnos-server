package controllers.api

import model.Category
import Const.Category.ID
import dao.CategoryDao
import spark.Request
import spark.Response
import utils.JdbiConfiguration
import utils.toJson
import java.sql.SQLException

/**
 * The object defining the API for the model.Category entity
 */
object CategoryApi{

    /**
     * Returns all the categories
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getAllCategories(request: Request, response: Response): String{
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Category>, CategoryDao, SQLException>(CategoryDao::class.java)
        { it.getAllCategories() }
                .toJson()
    }

    /**
     * Gets the category requested from its id
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getCategoryByID(request: Request, response: Response): String{
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Category>, CategoryDao, SQLException>(CategoryDao::class.java)
        { it.getCategoryById(request.params(ID).toInt()) }
                .toJson()
    }
}