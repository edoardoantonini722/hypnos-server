package controllers.api

import Const.Dream.ID
import Const.Dream.NEGATIVEIDS
import Const.DreamCat.CATEGORY_ID
import spark.Request
import spark.Response
import model.Dream
import model.DreamAddingPayload
import com.beust.klaxon.Klaxon
import dao.DreamDao
import utils.JdbiConfiguration
import utils.badRequest
import utils.okCreated
import utils.toJson
import java.sql.SQLException

/**
 * The object defining the API for the model.Dream entity
 */
object DreamApi{

    /**
     * Adds the passed dream to the database
     * @param [request] the http request
     * @param [response] the http response
     * @return the response code
     */
    fun addDream(request: Request, response: Response): String{
        val dreamPayload : DreamAddingPayload = Klaxon().parse<DreamAddingPayload>(
                request.body()) ?: return response.badRequest("Expected model.DreamAddingPayload json serialized object," +
                "found: ${request.body()}")
        JdbiConfiguration.INSTANCE.jdbi.useExtension<DreamDao, SQLException>(DreamDao::class.java) {
            val dreamId = it.addDream(dreamPayload.dream.text)
            for(item in dreamPayload.categoryList) it.addDreamCatRow(dreamId,item.id)
        }
        return response.okCreated()
    }

    /**
     * Returns all the dreams
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getAllDreams(request: Request, response: Response): String {
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Dream>, DreamDao, SQLException>(DreamDao::class.java)
        { it.getAllDreams() }
                .toJson()
    }

    /**
     * Gets the requested dream from its id
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getDreamById(request: Request, response: Response): String {
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Dream>, DreamDao, SQLException>(DreamDao::class.java)
        { it.getDreamById(request.params(ID).toInt()) }
                .toJson()
    }

    /**
     * Return the count of total dreams
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getDreamCount(request: Request, response: Response): String {
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<Int, DreamDao, SQLException>(DreamDao::class.java)
        { it.getDreamCount() }
                .toJson()
    }

    /**
     * Gets all the dreams contained in the passed category
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getDreamsByCategory(request: Request, response: Response): String {
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Dream>, DreamDao, SQLException>(DreamDao::class.java)
        { it.getDreamsByCategory(request.params(CATEGORY_ID).toInt()) }
                .toJson()
    }

    /**
     * Gets all the dreams that aren't part of none of the passed categories
     * @param [request] the http request
     * @param [response] the http response
     * @return the query result in Json
     */
    fun getDreamsByCategories(request: Request, response: Response): String {
        return JdbiConfiguration.INSTANCE.jdbi.withExtension<List<Dream>, DreamDao, SQLException>(DreamDao::class.java)
        {
            with(request){
                when(queryParams(NEGATIVEIDS)){
                    "" -> it.getAllDreams()
                    else -> it.getDreamsByCategories(queryParams(NEGATIVEIDS).split(',').map{it.toLong()})
                }
            }
        }.toJson()
    }
}