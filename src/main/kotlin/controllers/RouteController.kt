import Const.RestParams.APPLICATION_JSON
import Const.RestParams.PORT
import controllers.api.CategoryApi
import controllers.api.DreamApi
import spark.Spark.path
import spark.Spark.port
import spark.kotlin.get
import spark.kotlin.post

/**
 * The object defining the routes for the API
 */
object RouteController{

    fun init(){
        port(PORT)

        path("/${Const.Dream.API_NAME}"){
            get("",APPLICATION_JSON) { DreamApi.getAllDreams(request, response) }
            post("", APPLICATION_JSON) {DreamApi.addDream(request,response)}
            get("/categoryParams",APPLICATION_JSON) { DreamApi.getDreamsByCategories(request, response) }
            get("/count",APPLICATION_JSON) { DreamApi.getDreamCount(request, response) }
            get("/:${Const.Dream.ID}",APPLICATION_JSON) { DreamApi.getDreamById(request, response) }

            path("/${Const.Category.API_NAME}"){
                get("/:${Const.DreamCat.CATEGORY_ID}",APPLICATION_JSON) { DreamApi.getDreamsByCategory(request, response) }
            }
        }

        path("/${Const.Category.API_NAME}"){
            get("", APPLICATION_JSON) {CategoryApi.getAllCategories(request, response)}
            get("/:${Const.Category.ID}", APPLICATION_JSON) {CategoryApi.getCategoryByID(request, response)}
        }
    }
}