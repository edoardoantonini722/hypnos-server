package dao

import model.Category
import Const.Category.ID
import Const.Category.TABLE_NAME
import org.jdbi.v3.sqlobject.customizer.Bind
import org.jdbi.v3.sqlobject.statement.SqlQuery

/**
 * The model.Category Data Acces Object binding SQL queries to functions
 */
interface CategoryDao{

    /**
     * Selects all the categories
     * @return the list of categories
     */
    @SqlQuery("SELECT * FROM $TABLE_NAME")
    fun getAllCategories(): List<Category>

    /**
     * Selects a specific category by its id
     * @param [id] the category id
     * @return the list of categories matching the predicate (possibly one)
     */
    @SqlQuery("SELECT * FROM $TABLE_NAME WHERE $ID = (:$ID)")
    fun getCategoryById(@Bind(ID) id: Int): List<Category>
}