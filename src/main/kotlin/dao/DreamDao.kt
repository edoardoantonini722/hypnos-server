package dao

import Const.Dream.ID
import Const.Dream.NEGATIVEIDS
import Const.Dream.TABLE_NAME
import Const.Dream.TEXT
import Const.DreamCat.CATEGORY_ID
import Const.DreamCat.DREAM_ID
import org.jdbi.v3.sqlobject.statement.SqlQuery
import model.Dream
import org.jdbi.v3.sqlobject.customizer.Bind
import org.jdbi.v3.sqlobject.statement.SqlUpdate

/**
 * The model.Dream Data Access Object binding SQL queries to functions
 */
interface DreamDao{

    /**
     * Adds a dream
     * @param [text] the text of the dream
     * @return the dream id
     */
    @SqlQuery("INSERT INTO $TABLE_NAME($TEXT) VALUES (:$TEXT) RETURNING id")
    fun addDream(@Bind(TEXT) text: String): Int

    /**
     * Update the CatRow table to set the category for a dream
     * @param [dreamId] the dream id
     * @param [categoryId] the category id
     */
    @SqlUpdate("INSERT INTO ${Const.DreamCat.TABLE_NAME}($DREAM_ID,$CATEGORY_ID) VALUES (:$DREAM_ID,:$CATEGORY_ID)")
    fun addDreamCatRow(@Bind(DREAM_ID)dreamId: Int, @Bind(CATEGORY_ID) categoryId: Int)

    /**
     * Returns all dreams
     * @return the list of dreams
     */
    @SqlQuery("SELECT * FROM $TABLE_NAME " +
            "ORDER BY $ID")
    fun getAllDreams(): List<Dream>

    /**
     * Gets a dream by its id
     * @param [id] the dream id
     * @return the list of dreams matching the predicate(possibly one)
     */
    @SqlQuery("SELECT * FROM $TABLE_NAME WHERE $ID = (:$ID)")
    fun getDreamById(@Bind(ID) id: Int): List<Dream>

    /**
     * Counts the number of dreams
     * @return the count of dreams
     */
    @SqlQuery("SELECT count(*) FROM $TABLE_NAME")
    fun getDreamCount(): Int

    /**
     * Gets all the dreams in a specified category
     * @param [id] the categoryId
     * @return the list of dreams in that category
     */
    @SqlQuery("SELECT DISTINCT D.* FROM $TABLE_NAME AS D, ${Const.DreamCat.TABLE_NAME} as DC " +
            "WHERE D.$ID = DC.${Const.DreamCat.DREAM_ID} "+
            "AND DC.${Const.DreamCat.CATEGORY_ID} = (:$ID)")
    fun getDreamsByCategory(@Bind(ID) id: Int): List<Dream>

    /**
     * Gets all the dreams that aren't part of none of the passed categories
     * @param [negativeIds] the ids of the categories that the user won't see
     * @return the list of dreams not in those categories
     */
    @SqlQuery("SELECT DISTINCT D.* FROM $TABLE_NAME AS D, ${Const.DreamCat.TABLE_NAME} as DC " +
            "WHERE D.$ID = DC.${Const.DreamCat.DREAM_ID} " +
            "AND D.$ID NOT IN (" +
            "SELECT DISTINCT DC.${Const.DreamCat.DREAM_ID} " +
            "FROM ${Const.DreamCat.TABLE_NAME} as DC " +
            "WHERE DC.${Const.DreamCat.CATEGORY_ID} =  ANY(:$NEGATIVEIDS)) " +
            "ORDER BY D.$ID")
    fun getDreamsByCategories(@Bind(NEGATIVEIDS) negativeIds: List<Long>): List<Dream>


}
