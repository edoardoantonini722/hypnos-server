package utils

import spark.Response
import utils.toJson

/**
 * An utility class for response messages
 */

/**
 * A data class representing a response message
 * @param [id] the response code
 * @param [message] the response message
 */
data class ResponseMessage(val id: Int, val message: String)

/**
 * Extension function to set a 201 response
 * @return the response
 */
fun Response.okCreated(): String {
    this.status(201)
    this.type("application/json")
    this.body(ResponseMessage(201, "Ok Created").toJson())
    return this.body()
}

/**
 * Extension function to set a 400 response
 * @param [additionalInformation] the field when additional information can be set
 * @return the response
 */
fun Response.badRequest(additionalInformation: String?): String {
    this.status(400)
    this.type("application/json")
    this.body(ResponseMessage(400, "Request body was unacceptable. $additionalInformation?").toJson())
    return this.body()
}