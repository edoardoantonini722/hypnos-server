package utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * The Extensions function file
 */

/**
 * An object wrapping a Gson
 */
object GsonInitializer {
    private val gson:Gson = GsonBuilder().create()
    fun toJson(src: Any?): String = gson.toJson(src)
}

/**
 * Extension function to convert a generic object to Json
 */
fun Any.toJson(): String = GsonInitializer.toJson(this)
