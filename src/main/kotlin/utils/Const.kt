/**
 * A class containing various useful constants
 */

object Const{

    object RestParams{
        const val APPLICATION_JSON = "application/json"
        const val PORT = 8500
    }

    object Dream{
        const val TABLE_NAME = "dream"
        const val API_NAME = "dreams"
        const val ID = "id"
        const val TEXT = "text"
        const val POSITIVEIDS = "positiveids"
        const val NEGATIVEIDS = "negativeids"
    }

    object DreamCat{
        const val TABLE_NAME="dream_cat"
        const val DREAM_ID = "dreamid"
        const val CATEGORY_ID="categoryid"
    }

    object Category{
        const val TABLE_NAME="category"
        const val API_NAME="categories"
        const val ID = "id"
    }

    object Connection {
        val LOCAL_HOST = "127.0.0.1"
        val ADDRESS = LOCAL_HOST
        val PROTOCOL = "http"
        val PROTOCOL_SEPARATOR = "://"
        val PORT_SEPARATOR =":"
    }
}