CREATE TABLE Dream (
    Id BigSerial PRIMARY KEY,
    Text Text
);

CREATE TABLE Category (
    Id BigSerial PRIMARY KEY,
    Name VarChar(50)
);

CREATE TABLE Dream_Cat (
    DreamId BigInt,
    CategoryId BigInt,
    CONSTRAINT dream_cat_pkey PRIMARY KEY (dreamid, categoryid)
);