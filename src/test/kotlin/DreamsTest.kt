
import Const.Connection.ADDRESS
import Const.Connection.PORT_SEPARATOR
import Const.Connection.PROTOCOL
import Const.Connection.PROTOCOL_SEPARATOR
import Const.Dream.API_NAME
import Const.Dream.NEGATIVEIDS
import Const.Dream.POSITIVEIDS
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.result.Result
import model.Dream
import org.junit.BeforeClass
import org.junit.Test
import utils.JdbiConfiguration
import utils.toJson

class DreamsTest {


    companion object {

        private lateinit var addDream: String
        private lateinit var getAllDreams: String
        private lateinit var getDreamById: String
        private lateinit var getDreamsCount: String
        private lateinit var getDreamsByCategory: String
        private lateinit var getDreamsByCategories: String
        private var listResult = listOf<Dream>()

        @BeforeClass
        @JvmStatic
        fun setup() {
            RouteController.init()
            JdbiConfiguration.init()
            Thread.sleep(2000)
            addDream = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME"
            getAllDreams = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME"
            getDreamById = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME/2"
            getDreamsCount = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME/count"
            getDreamsByCategory = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME/${Const.Category.API_NAME}/4"
            getDreamsByCategories = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME/categoryParams?$POSITIVEIDS=1,5&$NEGATIVEIDS=2,3,4"
        }


    }

    @Test
    fun `get all dreams`() {
        listResult=handlingGetResponse(makeGet(getAllDreams))
        assert(listResult.size == 12)
    }


    @Test
    fun `get dream by id = 2`() {
        listResult=handlingGetResponse(makeGet(getDreamById))
        assert(listResult.size == 1)
        assert(listResult.first().text.contains("apocalisse"))
    }

    @Test
    fun `get dreams count`() {
        assert(getDreamsCount.httpGet().responseString().third.component1() == "12")
    }

    @Test
    fun `get dream by categoryId = 4`() {
        listResult=handlingGetResponse(makeGet(getDreamsByCategory))
        assert(listResult.size == 3)
    }

    @Test
    fun `get dream by categoryId != 2,3,4`() {
        listResult=handlingGetResponse(makeGet(getDreamsByCategories))
        assert(listResult.size == 6)
    }

    @Test
    fun `add dream`(){
        listResult=handlingGetResponse(makeGet(getAllDreams))
        val startingCount = listResult.size
        makePost(addDream,testDreamPayload.toJson())
        listResult=handlingGetResponse(makeGet(getAllDreams))
        assert(startingCount + 1 == listResult.size)

    }


    private fun makeGet(string: String): Triple<Request, Response, Result<String, FuelError>> {
        return string.httpGet().responseString()
    }

    private fun makePost(string: String, data: String): Triple<Request, Response, Result<String, FuelError>> {
        return string.httpPost().body(data).responseString().also {
            it.third.fold(success = {
                println(it)
            }, failure = {
                println(String(it.errorData))
            })
        }
    }
}