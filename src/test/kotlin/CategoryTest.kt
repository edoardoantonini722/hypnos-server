import Const.Category.API_NAME
import Const.Connection.ADDRESS
import Const.Connection.PORT_SEPARATOR
import Const.Connection.PROTOCOL
import Const.Connection.PROTOCOL_SEPARATOR
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import model.Category
import org.junit.BeforeClass
import org.junit.Test
import utils.JdbiConfiguration

class CategoryTest {


    companion object {

        private lateinit var getAllCategories: String
        private lateinit var getCategoryById: String
        private var listResult = listOf<Category>()

        @BeforeClass
        @JvmStatic
        fun setup() {
            RouteController.init()
            JdbiConfiguration.init()
            Thread.sleep(2000)
            getAllCategories = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME"
            getCategoryById = "$PROTOCOL$PROTOCOL_SEPARATOR$ADDRESS$PORT_SEPARATOR${Const.RestParams.PORT}/$API_NAME/2"
        }


    }

    @Test
    fun `get all categories`() {
        listResult = handlingGetResponse(makeGet(getAllCategories))
        assert(listResult.size == 5)
    }


    @Test
    fun `get category by id = 2`() {
        listResult = handlingGetResponse(makeGet(getCategoryById))
        assert(listResult.size == 1)
        assert(listResult.first().name.contains("Paralisi"))
    }

    private fun makeGet(string: String): Triple<Request, Response, Result<String, FuelError>> {
        return string.httpGet().responseString()
    }

}
