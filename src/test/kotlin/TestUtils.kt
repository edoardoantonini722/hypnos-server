import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.result.Result
import model.Category
import model.Dream
import model.DreamAddingPayload
import java.io.Reader
import java.io.StringReader
import java.util.*

/**
 * An utility class for the tests
 */

/**
 * A generic inline function used to parse a query result into a list
 * @param A the type of the list
 * @param [triplet] the triple containing request, response and result
 * @return the list
 */
inline fun <reified A> handlingGetResponse(triplet: Triple<Request, Response, Result<String, FuelError>>): List<A> {
    var listResult = listOf<A>()
    triplet.third.fold(success = {
        val klaxon = Klaxon()
        JsonReader(StringReader(it) as Reader).use { reader ->
            listResult = arrayListOf()
            reader.beginArray {
                while (reader.hasNext()) {
                    val data = klaxon.parse<A>(reader)!!
                    (listResult as ArrayList<A>).add(data)
                }
            }
        }
    }, failure = {
        println(String(it.errorData))
    })
    return listResult
}

val testDreamPayload = DreamAddingPayload(Dream(1, "a"), listOf(Category(2, "a"), Category(3, "b")))